<?php
/**
 *  Copyright 2016 Daniel Pöllmann
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

/*
 * Basic Constants
 */
const VERSION = 0.1;
const ALLOWED_MODES = array(
    'fl',
    'ffl',
    'fdl',
    'fde'
);

/*
 * Setup Argument Parsing
 */
$shortopts = "";
$shortopts .= "f:";
$shortopts .= "a::";
$shortopts .= "h::";
$shortopts .= "v::";
$shortopts .= "r::";
$shortopts .= "d::";
$shortopts .= "s::";
$shortopts .= "m::";
// Parse Arguments
$options = getopt($shortopts, array());

/*
 * Setup Constants based on arguments
 */
if (isset($options['v'])) {
    echo "VERSION: " . VERSION . "\n";
    exit();
}

if (isset($options['a'])) {
    echo "Possible hash algorithms:\n";
    foreach (hash_algos() as $v) {
        echo "    - $v\n";
    }
    exit();
}

if (isset($options['h'])) {
    if (in_array($options['h'], hash_algos())) {
        define("HASHALGO", $options['h']);
    } else {
        echo "Unsupported Hash algorithm: " . $options['h'];
        exit();
    }
} else {
    define("HASHALGO", "md5");
}

if (isset($options['r'])) {
    define("RECURSIVE", true);
} else {
    define("RECURSIVE", false);
}

if (isset($options['d'])) {
    define("AUTODELETE", true);
} else {
    define("AUTODELETE", false);
}

if (isset($options['s'])) {
    define("SIZEONLY", true);
} else {
    define("SIZEONLY", false);
}

if (isset($options['f'])) {
    if (is_dir($options['f'])) {
        define("FOLDER", $options['f']);
    } else {
        echo "This is not a directory: " . $options['f'] . "\n";
        exit();
    }
} else {
    echo "Please select a folder";
    exit();
}

if (isset($options['m'])) {
    if (in_array($options['m'], ALLOWED_MODES)) {
        define("MODUS", $options['m']);
    } else {
        echo "Mode not recognized.";
        exit();
    }
} else {
    define("MODUS", "mfl");
}

/*
 * Get Directory Content into $files
 */
if (RECURSIVE) {
    $files = getDirContents(FOLDER);
} else {
    $files = scandir(FOLDER);
    $temp_files = array();
    foreach ($files as $key => $value) {
        if (!is_dir($value) && !in_array($value, array(".", ".."))) {
            $temp_files[] = FOLDER . "/" . $value;
        }
    }
    $files = $temp_files;
}

/*
 * Calculate Hashes for the Files and save to array $hashes
 */
$hashes = array();
foreach ($files as $key => $file) {
    echo "Hashing: " . $file . "\n";
    $hash = hash_file(HASHALGO, $file);
    $hashes[$hash][] = $file;
}

/*
 * If Sizeonly
 */
if (SIZEONLY) {
    $size = 0;
    echo "\n\nThe following files WOULD be deleted permanently\n\n";
}

/*
 * Loop through all hashes and check if more than one file has the same hash value
 */
foreach ($hashes as $hash => $same_files) {

    if (count($same_files) > 1) {

        if (SIZEONLY) {

            // Get Array of files which should be deleted
            $delete = determineFilesToDelete($same_files);

            // Print files that will be deleted
            listFiles($delete);
            $size = $size + calculateFilesizeSum($delete);

        } else {

            if (AUTODELETE) {

                // Get Array of files which should be deleted and delete instantly
                deleteFiles(determineFilesToDelete($same_files));

            } else {

                // Clear CLI
                clear();

                // Print Files that have the same hash value
                echo "This files have the same hash value:\n";
                listFiles($same_files);

                // Get Array of files which should be deleted
                $delete = determineFilesToDelete($same_files);

                // Print files that will be deleted
                echo "\n\nThe following files will be deleted permanently\n\n";
                listFiles($delete);

                // Print free space
                echo "\n" . human_filesize(calculateFilesizeSum($delete), 4) . " will be freed after this operation\n";

                // Ask user for confirmation
                haltUser();

                // Delete files
                deleteFiles($delete);

            }

        }

    }

}

if (SIZEONLY) {
    echo "\n" . human_filesize($size, 4) . " WOULD be freed after this operation\n";
}

/*
 * Functions
 */

/**
 * Crawls a directory recursive
 * @param string $dir Filepath to directory
 * @param array $results Add files to this array
 * @return array Array containing all files
 */
function getDirContents($dir, &$results = array())
{
    $files = scandir($dir);

    foreach ($files as $key => $value) {
        $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
        if (!is_dir($path)) {
            $results[] = $path;
        } else if ($value != "." && $value != "..") {
            getDirContents($path, $results);
        }
    }

    return $results;
}

/**
 * Halt executing the program and ask the user to press ENTER to continue
 * and give him time to stop by pressing CTRL-C
 */
function haltUser()
{
    echo "\n";
    printf("Press CTRL-C to abort, enter to continue\n\n");
    $fp = fopen("php://stdin", "r");
    fgets($fp);
}

/**
 * Clears the Terminal screen
 */
function clear()
{
    passthru('clear');
}

/**
 * Determines which files should be deleted. Depends on the chosen mode (see -m)
 * @param array $duplicateFiles Array which contains files with the same hashvalue
 * @return mixed Array containing files which are chosen to be deleted
 */
function determineFilesToDelete($duplicateFiles)
{
    if (MODUS == 'fl') {
        $shortest_index = 0;
        foreach ($duplicateFiles as $key => $file) {
            if (strlen(basename($file)) < strlen(basename($duplicateFiles[$shortest_index]))) {
                $shortest_index = $key;
            }
        }
        unset($duplicateFiles[$shortest_index]);
        return $duplicateFiles;
    }

    if (MODUS == 'ffl') {
        $shortest_index = 0;
        foreach ($duplicateFiles as $key => $file) {
            if (strlen($file) < strlen($duplicateFiles[$shortest_index])) {
                $shortest_index = $key;
            }
        }
        unset($duplicateFiles[$shortest_index]);
        return $duplicateFiles;
    }

    if (MODUS == 'fdl') {
        $survive_index = 0;
        foreach ($duplicateFiles as $key => $file) {
            if (filemtime($file) > filemtime($duplicateFiles[$survive_index])) {
                $survive_index = $key;
            }
        }
        unset($duplicateFiles[$survive_index]);
        return $duplicateFiles;
    }

    if (MODUS == 'fde') {
        $survive_index = 0;
        foreach ($duplicateFiles as $key => $file) {
            if (filemtime($file) < filemtime($duplicateFiles[$survive_index])) {
                $survive_index = $key;
            }
        }
        unset($duplicateFiles[$survive_index]);
        return $duplicateFiles;
    }
    return array();
}

/**
 * Calculates the sum of the filesizes of all files in $files (in Bytes)
 * @param array $files Array which contains the files
 * @return int
 */
function calculateFilesizeSum($files)
{
    $size = 0;
    foreach ($files as $key => $file) {
        $size = $size + filesize($file);
    }
    return $size;
}

/**
 * Generates a human readable Filesize
 * @param int $bytes Amount of Bytes
 * @param int $decimals Amount of decimals
 * @return string human readable filesize
 */
function human_filesize($bytes, $decimals = 2)
{
    $sz = 'BKMGTP';
    $factor = floor((strlen($bytes) - 1) / 3);
    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[intval($factor)];
}

/**
 * Prints a list of files
 * @param array $files Files to be printed
 */
function listFiles($files)
{
    if ($files == array()) {
        echo "    - NONE -\n";
    } else {
        foreach ($files as $key => $file) {
            echo "    - $file\n";
        }
    }
}

/**
 * Deletes files
 * @param array $files Files to be deleted
 */
function deleteFiles($files)
{
    foreach ($files as $key => $file) {
        echo "DELETING: $file \n";
        unlink($file);
    }
}