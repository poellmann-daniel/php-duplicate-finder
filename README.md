# Duplicate Finder

This script is intended to find duplicate files, calculate the possible free space when deleted and if you want delete 
the files.

## Installation
Download `dup.php` and you're ready to go.

## Dependencies
Duplicate Finder relies on `PHP`.
 
## Usage
Call `dup.php` from the command line with `php dup.php [OPTIONS]`

## Options
* `-f` Sets the folder

* `-a` Prints allowed hash functions. (This can vary depending on your system.)

* `-h[hash]` Sets the used hash function. If not specified then `md5` is used.

* `-v` Prints the version of this tool. Ignores everthing else.

* `-r` Crawls through the directory recursively.

* `-d` Automatic deletion of duplicates - BE CAREFUL!

* `-s` Don't delete, don't prompt, just calculate the possible freed space and print files that would be deleted.

* `-f` Sets the folder.

* `-m[mode]` Sets the mode on how the program will decide which files to delete.

## Deletion Modes
Currently there are 4 deletion modes supported:

* `fl` Chooses longer filenames to be deleted. (Useful if duplicates have something like "Copy 1" added to filename) This is the standard.

* `ffl` Chooses longer filenames + filepath to be deleted. (Slashes are part of the length of filename + filepath.)

* `fdl` Chooses earliest files to be deleted. (Based on modify date and time.)

* `fde`Chooses latest files to be deleted. (Based on modify date and time.)

## Examples
Calculate how much _space_ can be freed up by deleting duplicates within the _current directory_. Do this _recursive_ and use _md5_.

`php dup.php -hmd5 -r -s -f.`

Don't find files recursive. Only the files in the directory at "top level".

`php dup.php -s -f.`

Delete the files automatically. Be careful and _NEVER_ run that as root.

`php dup.php -hmd5 -r -s -f. -d`

Ask before deletion and allow user to stop the program. (Standard Behavior)

`php dup.php -hmd5 -r -f.`

Use `sha512` for hashing.

`php dup.php -hsha512 -r -s -f.`

Work on `/home/daniel/Pictures`.

`php dup.php -hmde -r -f/home/daniel/Pictures`

Delete duplicates that were last modified.

`php dup.php -hmde -mfde -r -s -f/home/daniel/Pictures`

Print Version of this tool.

`php dup.php -v`

Get List of allowed Hashfunctions for the `-m` Flag.

`php dup.php -a`

## Licence
Apache 2, see `LICENCE`

If you need another licence, contact me.